---
title: "Ketika rekan kerjamu menolak sejalan dengamu"
draft: false
date: 2020-04-27
author: "Rengga Nugroho"
tags: ["team management, management"]
description: "Bagaimana jadinya ketika salah satu anggota timmu memiliki jalan yang berbeda denganmu ? Apa yang akan kamu lakukan?"
type: post
---
Tadi saya dapat bahan diskusi dari salah satu grub komunitas saya, dan saya coba untuk tulis disini sebagai arsipnya..

> Question
> Apa yang akan kamu lakukan ketika salah satu rekan kerjamu tidak sejalan/bertolak belakang dengan tim karena suatu masalah?

Yang perlu digaris bawah dari masalah ini adalah *Masalah tertentu* dan *tidak sejalan / bertolak belakang*. Menurut saya pribadi, bagian tidak sejalan dan bertolak belakang memiliki pengertian yang sangat berbeda dan perlu diketahui apa yang menjadi dasar masalah.

## What is this and these_

### Penjelasan Singkat Bertolak belakang

Secara sempit, bertolak belakang berarti **menolak**, menunjukkan ketidak setujuan, sangat berbeda. Dapat diibaratkan kita pergi ke barat, tetapi mereka ke timur.

Dengan demikian, mereka yang tidak sejalan biasanya dikarenakan adanya suatu keputusan maupun rencana yang menurut mereka **tidak cocok dengan jalan pemikirannya**. Bisa diakibatkan karena terlalu *tertutupnya informasi* yang diberikan, informasi yang kabur maupun hanya sekedar angin lewat. Sehingga mereka yang bertolak belakang akan menolak segala hal yang berkaitan dengan keputusan atau rencana.

### Penjelasan Singkat Tidak sejalan

Its quite interesting here, bisa dimaksudkan mengambil jalan yang berbeda, maupun bertolak belakang. Tapi ada kemungkinan menjadi seperti ini: Mereka setuju dengan tujuan utama dari rencana tersebut, tetapi mereka **menyelesaikan dengan cara yang berbeda** dengan yang disepakati. Dapat diibaratan seperti "Kita pergi ke titik A, melalui jalur X, tetapi mereka mengambil jalur Y karena dianggap lebih cepat daripada jalur X, tetapi masih dengan tujuan titik A.

Hal ini sering terjadi terutama pada rencana jangka panjang yang memiliki proses panjang, serta dampak dari proses terseut tidak langsung terasa. Sehingga *kejenuhan dan rasa bosan* membuat beberapa orang **mengambil Jalan Tol/Shortcut**. Kasus ini terbilang cepat diatasi dengan cara menyesuikan rencana serta plan agar sesuai dengan keadaan kelompok, lingkungan serta dapat memecahkan masalah yang sedang dihadapi.

## Trus gimana cara ngatasinya

Tidak akan mudah jika tetapi memasang ego kalian bahwa rencana anda sudah sempurna.

### Its hard to find a punch hole right now

Try to communicate with them extensively. Ask them about their feelings and opinions. Recall the plan and discuss across with others. Ask to change strategy and manuvers.

Tidak serta merta membuang anggota bermasalah dapat menyelesaikan masalah! Cobak cek kembali rencana dan strategi, cek dan cek kembali. Kenapa? Ada kemungkinan sangat besar bahwa rencana tersebut **SALAH DAN SUDAH TIDAK COCOK UNTUK DILAKUKAN**. Keputusan, rencana, kebijakan hanya benar, cocok ketika diputuskan.

Pahami pain-points dan masalah yang mereka utarakan. Kalian sedang menangani orang yang sangat kritis (benar benar kritis). Jangan menyalahkan mereka yang menentangmu karena alasan yang logis. Teruslah pahami apa yang mereka rasakan.

Sayangnya, tidak ada win-win solution. Kalian hanya memiliki opsi "BUANG, KEEP, CHANGE STRATEGY".

### Kok ribet sih gaada yang mudah gtu jelasinnya?

Sigh, okay..

> Ajak Komunikasi dengan serius, Dengarkan pendapat mereka tanpa menyela, Validasi masalah yang diutarakan dengan anggota lainnya, timbang solusi yang diberikan oleh para anggota, Pengambilan keputusan.

## Oiya, masalah tertentunya gimana?

Whatever is that, I don't think it much. Cuz, It may personal problem rather than community lack. So, Let's ignore it UmmU

## Conclusion

Mereka yang bermasalah dengan alasan logis adalah orang yang sangat kritis karena dapat melihat kejanggalan dan dapat menemukan duct-tape solution.

## What will you do

Ah, I forgot it... Sorry...

> Saya akan komunikasi dengan mereka, setelah dengar pendapat, saya meminta kejelasan dari leader untuk memvalidasi apakah benar yang dikatakan oleh mereka. Saya menjelaskan keadaan sementara dan meminta leader untuk turun tangan sebagai penengah dalam diskusi, serta mengkroscek pembicaraan. dan decision making.

_Finished?_

Yeah.. Thanks...
