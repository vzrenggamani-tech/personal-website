---
title: "Bekerja dalam tekanan"
draft: false
date: 2020-04-12
author: "Rengga Prakoso Nugroho"
tags: ["kerja, tekanan, tips, trik"]
description: "Membahas bagaimana mengurangi tekanan dalam bekerja untuk diri sendiri. Catatan singkat pribadi saya sebelum memlai kerja dan disaat mengalami tekanan."
type: post
image: ""
---
# Mengurangi Tekanan Dalam Bekerja

Ini Pribadi sih tipsnya, sesuaikan dengan kamu dan lingkungan kerja,

### BERANI BERKATA TIDAK.TIDAK.TIDAK.dan.TIDAK
Hal wajib dalam bekerja adalah berani berkata tidak. Dalam hal apapun jika kamu tidak sanggup makan TOLAK-lah. Ini membuat hidupmu lebih mudah dan lebih nyaman. Jika memang kamu tidak sanggup maka tolak dan berkata tidak, dengan baik dan benar. Jangan anggap semua pekerjaan dapat kamu terima dan terus terima tanpa memikirkan hal pribadimu. Jangan anggap itu tantangan atau apa, kamu sebenarnya akan menyiksa dirimu. Jika memang tidak sanggup, maka TOLAK dan berkata TIDAK. Ingat, kamu punya hak untuk menolak yang bukan kewajiban utamamu. Jangan menambah sesuatu yang sudah tidak sanggup kamu tampung.

### Ketahui pekerjaanmu dan panduanya
Jangan sampai kamu bekerja tetapi tidak tahu apa yang akan kamu kerjakan. Sebelum bekerja, baca panduan atau BERTANYALAH PADA TEMANMU. WAJIB! Apapun itu, ketahui apa yang akan kamu kerjakan. Hal ini akan mengurangi resiko tertekan karena mengetahui bagaimana mengerjakan pekerjaan tersebut. Hal yang sering terjadi adalah bekerja tanpa membaca panduan, sehingga sering terjadi kebingunan di tengah jalan. Mengapa ? Karena tidak mengetahui bagaimana langkah-langkah dan best-practice pekerjaan tersebut.

### Tentukan Tujuan Utama
Yang sering terjadi adalah berfokus pada hal-hal kecil dan melupakan tujuan utama. Misalkan, jika kamu ingin menyelesaikan sebuah artikel, maka tujuan utama adalah menyelesaikan artikel tersebut dengan tuntas. Jangan berfokus pada hal-hal kecil seperti font, ukuran kertas, dan lainnya. Fokus untuk selesaikan masalah/tujuan utama. Hal-hal kecil tersebut dapat diselesaikan setelah masalah/tujuan utama selesai. Dengan begitu, fokus kerja tidak akan terbagi bagi dan tidak membebani dirimu. Tentukan tujuan utama dan selesaikan. Jangan terlalu berfokus pada hal-hal kecil.

### KAMU PUNYA TEMAN
KAMU PUNYA TEMAN.REKAN KERJA.TIM.MANAJER. Gunakanlah sebaik mungkin! Jangan malu untuk bertanya! Meminta mereka menurunkan tempo kerja. Minta mereka mengerjakan tugasmu sebagian kecil tugasmu. Mintalah saran dan saran. Tolong, Kamu memiliki teman, jadi gunakanlah sebaik mungkin. Kamu pasti bekerja dengan tim, kamu tidak bisa bekerja sendiri. Komunikasi dan Tim.

### Memulai ulang
Ketika semua sudah diluar kontrol, mulai ulang. Kenali pekerjaanmu sekali lagi, mungkin ada hal yang terlewat. Cek ulang pekerjaanmu. Baca kembali perlahan. Start dari nol. Minta temanmu untuk mengkoreksi ulang.

### Buat lingkungan kerja yang nyaman
Ya! Jika kamu suka musik, bekerjalah sembari mendengarkan musik. Kamu suka untuk bergerak, bekerjalah dengan berdiri. Intinya, buat lingkungan yang dapat membantu kamu rileks dalam bekerja tanpa menggangu orang lain. Jangan sampai kamu membawa gorilla ke kantor ketika semua orang takut dengan gorilla.

### Istiraha
Ambil waktu istirahat sebaik mungkin. Bercengkrama dengan teman-teman. Tonton video konyol atau hal yang kamu sukai. Intinya, lepaskan sebentar urusan pekerjaan untuk merefresh pikiran. Kamu bahkan bisa melakukan evaluasi mandiri disaat istirahat. Tolong, jangan melupakan istirahat.

yang terakhir

### ingat demi apa kamu bekerja
Keluarga?Pacar?Komunitas?Prinsip? Terserah. Tanamkan di dalam dirimu, jika kamu menyerah kamu akan mengecewakan mereka. 

### Akhir kata
Cuma ini sih, meskipun setertekan apapun dalam bekerja, masih bisa saya kontrol tempo dan rasa tertekan saya. Meskipun tidak semaksimal pada saat awal bekerja, tetapi tidak sampai membuat saya drop di tengah jalan.

Rengga Nugroho Sidoarjo