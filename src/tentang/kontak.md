---
title: "Social Media Listing and Contacts"
draft: false
date: 2020-1-4
author: "Rengga Prakoso Nugroho"
type: post
---

# Personal Account

**Email**<br>
[hi@renggaprakosonugroho.my.id](mailto:hi@renggaprakosonugroho.my.id)

**Twitter**<br>
[Rengga Prakoso Nugroho (@vzrenggamani)](https://twitter.com/vzrenggamani)

**Linkedin**<br>
[Rengga Prakoso Nugroho](https://linkedin.com/in/vzrenggamani)

**Quora**<br>
[Rengga Prakoso Nugroho](https://id.quora.com/Rengga-Prakoso-Nugroho)

**GitHub**<br>
[Rengga Prakoso Nugroho (@vzrenggamani)](https://github.com/vzrenggamani)

**GitLab**<br>
[Rengga Prakoso Nugroho (@vzrenggamani)](https://gitlab.com/vzrenggamani)

**Instagram**<br>
[Rengga Prakoso Nugroho (@vzrenggamani)](https://instagram.com/vzrenggamani)

# Owned Platform but not mainly used

**Medium**<br>
[Rengga Prakoso Nugroho](https://medium.com/@vzrenggamani)

**Launchpad**<br>
[Rengga Prakoso Nugroho (~vzrenggamani)](https://launchpad.net/~vzrenggamani)

**Facebook**<br>
[Rengga Prakoso Nugroho(vzrenggamani)](https://facebook.com/RenggaPrakosoNugroho)

**Academia.edu**<br>
[Rengga Prakoso Nugroho's academic articles](https://independent.academia.edu/vzrenggamani)

**Dicoding Profile**<br>
[Rengga Prakoso Nugroho coding courses](https://www.dicoding.com/users/144983)

**Pinterest Profile**<br>
[Rengga's Pins and favorites images](https://id.pinterest.com/vzrenggamani/)

**Stackoverflow**<br>
[Rengga's coding problems and stucksss](https://stackoverflow.com/users/9879735/rengga-prakoso-nugroho)