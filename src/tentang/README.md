---
home: true
heroImage: img/banner-web.jpg
heroAlt: Picture of Rengga Prakoso Nugroho
heroText: Rengga Prakoso Nugroho
tagline: I do human centered design.
actionText: Download Resume
actionLink: https://storage.renggaprakosonugroho.my.id/document/RenggaNugroho_CV-Resume.pdf
footer: Locally crafted with ♥️
pageClass: about
---

# About me

I'm college students of the University of Malang. I am majoring in Education technology degree. I spent my free time on programming and chilling out with my friends.

I passionate about technology, culture and technology development. I love to help everybody learn about the technology that improves social learning and education. I have dreams to make education are open to everyone.

Oh yeah, I born on Sidoarjo. Lovely place in Indonesia 🇮🇩.

Feel free to [Contact me!](/tentang/kontak)

<br>

## Past Experiences

*All past experiences are obtained ongoing with student status*

### Technology Officer

**SMA Negeri 1 Wonoayu**<br>
*2018 - 2020*<br>
Took part in sustaining the availability of intranet. Designed graphic and material for online publications. Directed picture-taking members to take part in events documentation.
<br>

### Graphic and Technology Lead

**SMA Negeri 1 Wonoayu Student Council**<br>
*2017 - 2020*<br>
Responsible for administration and digital archives of the student council. Founded ITC to be a playground for Information Technology Experiment. Led the development of websites and students database.
<br>

### Creative Director

**SMA Negeri 1 Wonoayu Photography Club**<br>
*2019*<br>
Acted with the school magazine club and student council to produce a stunning photograph to be published in social media. Crafted members ideas to be a monthly project. Led photographer crew in school's events.
<br>

### Editor

**SMA Negeri 1 Wonoayu Magazine Club**<br>
*2017 - 2018*<br>
Curated submission sent by students to be written in the magazine. Format various set of students assignments and design to make consistency format of documents.

<br>

## Education

### SMA Negeri 1 Wonoayu

Graduated on July, 2020.<br>
**Social and Cultural Major**, Speciality in **Japanaese Language**<br>
No GPA because of Corona Virus outbreak in Indonesia

### SMP Negeri 2 Wonoayu

Graduated on July, 2017<br>
**Regular Major**

<br>

## Certifications

### IBM
[**Enterprise Design Thinking Practitioner**](https://www.youracclaim.com/badges/3f84b95e-bdaf-4f87-85a2-c230cac79449/linked_in_profile) *February 2020*

### Coursera
[**Architecting with Google Kubernetes Engine Specialization**](https://www.coursera.org/account/accomplishments/specialization/certificate/HPET3F9H2KP9) *February 2020*<br>
[**Architecting with Google Kubernetes Engine: Production**](https://www.coursera.org/account/accomplishments/certificate/85RYDMUQ5GU3) *February 2020*<br>
[**Architecting with Google Kubernetes Engine: Workloads**](https://www.coursera.org/account/accomplishments/certificate/Q3DJEF4WMZ68) *February 2020*<br>
[**Architecting with Google Kubernetes Engine: Foundations**](https://www.coursera.org/account/accomplishments/certificate/UGQRZ68P7R8N) *February 2020*<br>
[**Google Cloud Platform Fundamentals: Core Infrastructure**](https://www.coursera.org/account/accomplishments/certificate/S9CHBRV8QYW7) *February 2020*<br>

### Dicoding Indonesia
[**Belajar Dasar Dasar Azure Cloud**](https://www.dicoding.com/users/144983) *October 2019*<br>
[**Memulai Pemrograman Dengan Python**](https://www.dicoding.com/users/144983) *April 2020*<br>

### Qwiklabs
[**GCP Essentials**](https://google.qwiklabs.com/public_profiles/9aba2472-2d1b-4b9e-aa01-bde1c24afeb8) *December 2019*<br>
[**GSuite Essentials**](https://google.qwiklabs.com/public_profiles/9aba2472-2d1b-4b9e-aa01-bde1c24afeb8) *December 2019*<br>
[**Website and Web Applications**](https://google.qwiklabs.com/public_profiles/9aba2472-2d1b-4b9e-aa01-bde1c24afeb8) *December 2019*<br>
[**Google Developer Essentials**](https://google.qwiklabs.com/public_profiles/9aba2472-2d1b-4b9e-aa01-bde1c24afeb8) *December 2019*<br>

<style>
.tentang-page {
    margin: auto;
    max-width: 740px;
    padding: 0px;
}
a {
    color: #2c3e50
}
</style>
