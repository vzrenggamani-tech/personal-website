const feed_options = {
    // Used for Feed plugins
    title: "Rengga Prakoso Nugroho Feeds",
    description: "Rengga Nugroho's Feeds",
    id: "http://renggaprakosonugroho.my.id/",
    link: "http://renggaprakosonugroho.my.id/",
    image: "http://renggaprakosonugroho.my.id/img/banner.jpg",
    favicon: "http://renggaprakosonugroho.my.id/favicon.ico",
    copyright: "All rights reserved 2020, Rengga Nugroho",
    // updated: new Date(2020, 3, 24), // optional, default = today
    // generator: "Feed for node.js", // optional, default = 'Feed for Node.js'
    feedLinks: {
        json: "https://renggaprakosonugroho.my.id/json",
        atom: "https://renggaprakosonugroho.my.id/atom"
    },
    author: {
        name: "Rengga Prakoso Nugroho",
        email: "hi@renggaprakosonugroho.my.id",
        link: "https://renggaprakosonugroho.my.id"
    }

};

module.exports = {
    dest: 'public',
    title: 'Rengga Nugroho - Graphic designer based in Sidoarjo',
    description: 'Rengga Prakoso Nugroho is a graphic designer based in Sidoarjo. Lead the Sakura Fukoka Studio. Studies social culture major.',
    themeConfig: {
        nav: [{
                text: 'Beranda',
                link: '/'
            },
            {
                text: 'Artikel',
                link: '/artikel/'
            },
            {
                text: 'Tentang',
                link: '/tentang/'
            }

        ],
        logo: '/img/logo-name.svg'
    },
    head: [
        ['meta', { name: 'keywords', content: "Rengga Prakoso Nugroho" }],
        ['meta', { name: 'title', content: "Rengga Prakoso Nugroho &bull; Graphic Designer based in Sidoarjo" }],
        ['meta', { 'http-equiv': 'Content-Type', content: "text/html" }],
        ['link', { rel: 'icon', href: "https://renggaprakosonugroho.my.id/favicon.ico" }],
        ['link', { rel: 'alternate', hreflang: "id", href: "https://renggaprakosonugroho.my.id" }],
        ['meta', { property: 'og:locale', content: "en_id" }],
        ['meta', { property: 'og:type', content: "website" }],
        ['meta', { property: 'og:title', content: "Rengga Prakoso Nugroho &bull; Graphic Designer based in Sidoarjo" }],
        ['meta', { property: 'og:description', content: "Graphic Designer based in Sidoarjo. I Do Human Centered Design" }],
        ['meta', { property: 'og:url', content: "https://renggaprakosonugroho.my.id" }],
        ['meta', { property: 'og:site_name', content: "Rengga Prakoso Nugroho blog and life journey" }],
        ['meta', { property: 'og:image', content: "https://renggaprakosonugroho.my.id/img/banner.jpg" }],
        ['meta', { property: 'og:image:secure_url', content: "https://renggaprakosonugroho.my.id/img/banner.jpg" }],
        ['meta', { property: 'og:image:width', content: "1200" }],
        ['meta', { property: 'og:image:height', content: "630" }],
        ['meta', { property: 'og:profile:first_name', content: "Rengga" }],
        ['meta', { property: 'og:profile:last_name', content: "Nugroho" }],
        ['meta', { property: 'og:profile:username', content: "vzrenggamani" }],
        ['meta', { property: 'article:author', content: "https://facebook.com/renggaprakosonugroho" }],
        ['meta', { name: 'twitter:card', content: "summary_large_image" }],
        ['meta', { name: 'twitter:description', content: "Graphic Designer based in Sidoarjo. I Do Human Centered Design" }],
        ['meta', { name: 'twitter:title', content: "Rengga Prakoso Nugroho &bull; Graphic Designer based in Sidoarjo" }],
        ['meta', { name: 'twitter:site', content: "@vzrenggamani" }],
        ['meta', { name: 'twitter:creator', content: "vzrenggamani" }],
        ['meta', { name: 'twitter:image', content: "https://renggaprakosonugroho.my.id/img/banner.jpg" }]
    ],
    plugins: [
        ['@vuepress/google-analytics', { 'ga': 'UA-68615481-7' }],
        ['feed', feed_options ],
        ['sitemap', { hostname: 'https://renggaprakosonugroho.my.id'}]
    ]
}