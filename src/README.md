---
home: true
heroImage: img/banner-web.jpg
heroAlt: Picture of Rengga Prakoso Nugroho
heroText: I do human centered design.
tagline: As you love it, Go on. Design for saving peoples lives. Not for burning their money.
actionText: About me →
actionLink: /tentang/
features:
  - title: Junior UI/UX Designer
    details:
  - title: Human Centered Design
    details: 
  - title: Junior UI/UX Writer
    details:
footer: Locally crafted with ♥️
pageClass: homepage
---